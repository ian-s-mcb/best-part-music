# best-part-music

"Every song, movie, story, has a best part", as told by Ellie Chu in the
Netflix film [The Half of It][film-wiki-link] from 2020. In this repo, I
document the best parts of every album I've reviewed, organized by month
and year, since 2018.

### Origin of repo name

These two, under 60-second YouTube clips show where this repo name came
from.

[![Video clip of Ellie and her dad in front of their home TV](https://img.youtube.com/vi/rTpTsPBWZP8/0.jpg)](https://www.youtube.com/watch?v=rTpTsPBWZP8)

[![Video clip of Ellie and Aster in a hot spring](https://img.youtube.com/vi/KBAzdgqusSc/0.jpg)](https://www.youtube.com/watch?v=KBAzdgqusSc)

### Motivation

So you've found a great album, and since you've been playing it for days
days now, you know just which tracks to put on your playlists and which
tracks to skip. Fast-forward a couple months, when you're in mood for a
recognizable favorite, you decide to put on that album, but you
hesitate...

What are all these track titles and which of these songs really
connected with me? There's no way I'm going to comb through them all to
find my place again. Alas, memory is so unreliable!

Well, you might be thinking, what about those buttons on my preferred
music streaming service for love and dislike? Ah yes, but how well does
that service let you browse the info from those buttons? And what if
those buttons are too imprecise to capture your feelings about an album?

Meet the **best-part-music** repo - a better way to retain that precious
sense of knowing a fully vetted album. Sure, it's just a bunch of
markdown files with a consistent convention for noting a song's rating,
but it works.

[film-wiki-link]: https://en.wikipedia.org/wiki/The_Half_of_It
